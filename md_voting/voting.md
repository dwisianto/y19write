# Decision Theory Voting

## Equation

(@foo)  $a^2 + b^2 = c^2$

As (@foo) says, ...

(@bar)  $e = x + y$

## Table

See table \ref{my_table}.

Table: (table title) \label{my_table}

-----------------------
Header1 | Header2 | Header3
------- |  ------- |  -------
item1   | item2    | item3
-----------------------

## Bibliography


